public class Bowler {
  public static final int pins   = 10;
  
  public static int score(final String name, final int[] rolls) {    
    int score = 0;
    int curRoll = 0;
    
    Scoreboard.printRolls(name,rolls);
    
    // Add frames 1-9 to score
    do {
      score+=rolls[curRoll];
      
      // Strike
      if(rolls[curRoll]==pins) {
        score+=rolls[curRoll+1]+rolls[curRoll+2];
        curRoll+=1;
      // Spare
      } else if(rolls[curRoll]+rolls[curRoll+1]==pins) {
        score+=rolls[curRoll+1]+rolls[curRoll+2];
        curRoll+=2;
      // Open 
      } else {
        score+=rolls[curRoll+1];
        curRoll+=2;
      }
      
      Scoreboard.printScore(score);
    } while(curRoll < rolls.length-3);
    
    // Add third frame to score
    score+=rolls[rolls.length-2]+rolls[rolls.length-1];
    // Three rolls in final frame? 
    if(rolls[rolls.length-3]==pins||rolls[rolls.length-3]+rolls[rolls.length-2]==pins) {
      score+=rolls[rolls.length-3];
    }
    
    Scoreboard.printFinalScore(score);

    return 0;
  }
}
