public class Scoreboard {
  private static final int nameLength = 6;
  private static final String nameFormat = "%-"+nameLength+"s";
  public static final int numFrames = 10;
  public static final int numRolls = numFrames*2 + 1 + 1;
  
  private static int[] expandRolls(final int[] rolls) {
    int[] expanded = new int[numRolls];
    
    System.arraycopy(rolls, 0, expanded, 0, rolls.length);
   
    // look for strikes except for in last frame
    for(int i = 0; i < expanded.length-4; i++) {
      // current roll is a strike, insert second roll placeholder
      if(i % 2 == 0 && expanded[i] == Bowler.pins) {
        System.arraycopy(expanded, i+1, expanded, i+2, expanded.length-i-2);
        expanded[i+1] = 0;
      }
      
    }
    
    return expanded;
  }
  
  public static void printFrames() {
    System.out.printf(nameFormat, " ");
    
    for(int i = 1; i < numFrames; i++) {
      System.out.printf("|%-6d", i);
    }
    
    System.out.printf("|%-9d|%n",numFrames);
  }
  
  // print rolls in a grid format
  public static void printRolls(final String name, final int[] rolls) {
    int[] expanded = expandRolls(rolls);
    
    System.out.printf(nameFormat, name.substring(0,nameLength));
    
    for(int i = 0; i < expanded.length-4; i+=2) {
      // Strike
      if(expanded[i]==Bowler.pins) {
        System.out.printf("|%3s%3s","X"," ");
      // Spare
      } else if(expanded[i]+expanded[i+1] == Bowler.pins) {
        System.out.printf("|%3s%3s", printRoll(expanded[i]),"/");
      // Open
      } else {
        System.out.printf("|%3s%3s", printRoll(expanded[i]),printRoll(expanded[i+1]));
      }
    }
    
    // Strike
    if(expanded[expanded.length-4] == Bowler.pins) {
      System.out.printf("|%3s", "X");
      // Double
      if(expanded[expanded.length-3] == Bowler.pins) {
        System.out.printf("%3s","X");
        // Turkey
        if(expanded[expanded.length-2] == Bowler.pins) {
          System.out.printf("%3s|%n", "X");         
        } else {
          System.out.printf("%3s|%n", printRoll(expanded[expanded.length-2]));
        }
      // Open second roll, no third roll
      } else {
        System.out.printf("%3s%3s|%n", printRoll(expanded[expanded.length-2])," ");
      }
    // Spare in second roll
    } else if (expanded[expanded.length-4]+expanded[expanded.length-3] == Bowler.pins) {
      System.out.printf("|%3s%3s", printRoll(expanded[expanded.length-4]), "/");
      // Strike on third roll
      if(expanded[expanded.length-2] == Bowler.pins) {
        System.out.printf("%3s|%n","X");
      } else {
        System.out.printf("%3s|%n", printRoll(expanded[expanded.length-2]));
      }
    // Open on second roll
    } else {
      System.out.printf("|%3s%3s%3s|%n", printRoll(expanded[expanded.length-4]), printRoll(expanded[expanded.length-4]), " ");
    }
    
    System.out.printf(nameFormat, " ");
  }
  
  // print a number of Bowler.pins or "-" on a roll of 0
  public static String printRoll(final int roll) {
    return roll==0?"-":Integer.toString(roll);
  }
  
  public static void printScore(final int score) {
    System.out.printf("|%6d", score);
  }
  
  public static void printFinalScore(final int score) {
    System.out.printf("|%9d|%n", score);
  }
}
