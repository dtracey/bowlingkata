public class BowlerTest {

	public static void main(String[] args) {
		int[] rolls = {6, 3, 8, 0, 8, 2, 7, 2, 5, 5, 9, 0, 9, 1, 10, 8, 2, 9, 1,7};
		int[] rolls2 = {4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4};
		int[] rolls3 = {5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5};
		int[] almost = {10,10,10,10,9,1,10,10,10,10,10,10,10};
		int[] perfect = {10,10,10,10,10,10,10,10,10,10,10,10};
		
		Scoreboard.printFrames();
		Bowler.score("David ",rolls);
		Bowler.score("Connor",rolls2);
		Bowler.score("Logan ",rolls3);
		Bowler.score("Josh  ",almost);
		Bowler.score("Andrew",perfect);
	}
}
